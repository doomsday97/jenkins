package com.abhishek.hibernatedocker.controller;


import com.abhishek.hibernatedocker.entities.Note;
import com.abhishek.hibernatedocker.services.hibernate_services;
import org.apache.catalina.filters.ExpiresFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.Servlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
public class NoteController {
    @Autowired
    hibernate_services hibernate_services;


    @GetMapping("/notes")
    public List<Note> getAllNotes() {
        return hibernate_services.getallnotes();
    }

    @PostMapping(value = "/notes")//, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Note createNote (@Valid @RequestBody Note note){
        System.out.println("Addition performed successfully");
        return hibernate_services.createNote(note);
    }

    @GetMapping("/notes/{id}")
    public Note getNoteById(@PathVariable(value = "id") long noteid){
        return hibernate_services.getNoteById(noteid);
    }

    @DeleteMapping("/notes/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long noteId) {
        System.out.println("deletion performed successfully");
        return hibernate_services.deleteNote(noteId);
    }


    @PutMapping(value = "/notes/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Note updateNote(@PathVariable(value = "id") Long noteId,
                           @Valid @RequestBody Note noteDetails) {
        System.out.println("updation performed successfully");
        return hibernate_services.updateNote(noteId, noteDetails);

    }

}
