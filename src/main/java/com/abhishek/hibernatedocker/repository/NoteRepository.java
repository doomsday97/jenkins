package com.abhishek.hibernatedocker.repository;

import com.abhishek.hibernatedocker.entities.Note;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoteRepository extends JpaRepository<Note, Long> {
}
