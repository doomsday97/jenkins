Feature: Update Note

  Scenario:
      Given check data exist for "http://localhost:8082/notes/20"
      When Update for existing object
      Then Response Body title should be "Cucumber"

  Scenario: get data
    Given check data  for "http://localhost:8082/notes"
    When get for object "http://localhost:8082/notes/14"
    Then Response Body "Cucumber"


  Scenario: delete data
    Given check data for delete "http://localhost:8082/notes"
    When delete object for "http://localhost:8082/notes/17"
    Then the response code should be "200"


  Scenario: Post data
    Given check data for create "http://localhost:8082/notes"
    When create new object for "http://localhost:8082/notes"
    Then Response Body id should be "32"