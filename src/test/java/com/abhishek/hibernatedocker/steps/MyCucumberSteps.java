package com.abhishek.hibernatedocker.steps;

import com.abhishek.hibernatedocker.entities.Note;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class MyCucumberSteps {
    String url;
    Note note = new Note();
    Note getnote= new Note();
    Note finalNote = new Note();
    RestTemplate restTemplate = new RestTemplate();


    // update method
        @Given("^check data exist for \"([^\"]*)\"$")
        public Note performPutOperationFor(String url){
           this.url = url;
            ResponseEntity<Note> response = restTemplate.getForEntity(url, Note.class);
            note = response.getBody();
            return note;
        }

        @When("^Update for existing object$")
        public void updateForExistingObject(){
            Note updatedNote = new Note();
            updatedNote.setTitle("Cucumber");
            updatedNote.setContent("Abhishek");
            if(note != null){
               restTemplate.put(this.url, updatedNote);
            }
        }

        @Then("^Response Body title should be \"([^\"]*)\"$")
        public void responseBodyTitleShouldBe(String title){
           finalNote = performPutOperationFor(this.url);
            Assert.assertEquals(title,finalNote.getTitle());
        }

        // get all name


    @Given("check data  for {string}")
    public Note checkDataFor(String url) {
        ResponseEntity<Note[]> response = restTemplate.getForEntity(url, Note[].class);
        note = response.getBody()[0];
        return note;
    }

    @When("get for object {string}")
    public Note getForObject(String url) {
        this.url = url;
        ResponseEntity<Note> response = restTemplate.getForEntity(url, Note.class);
        getnote = response.getBody();
        return getnote;
    }


    @Then("Response Body {string}")
    public void responseBody(String title) {
        restTemplate.getForEntity(url,Note.class);
        Assert.assertEquals(getnote.getTitle(), title);
    }

    // create node
    int a;
    Note createNote = new Note();

    @Given("check data for create {string}")
    public void checkDataForCreate(String url) {
        this.url = url;
        ResponseEntity<Note[]> response = restTemplate.getForEntity(url, Note[].class);
        note = response.getBody()[0];
    }



    @When("create new object for {string}")
    public void createNewObjectFor(String url) {
        Note notes = new Note( 31,"Abhishek", "Java");
        ResponseEntity<Note> result = restTemplate.postForEntity(url, notes, Note.class);
        a = result.getStatusCodeValue();
        createNote =  result.getBody();
    }



    @Then("Response Body id should be {string}")
    public void responseBodyIdShouldBe(String id) {
        Assert.assertEquals(createNote.getId(),Long.valueOf(id));
    }


    //delete note

    int code =0;
    @Given("check data for delete {string}")
    public void checkDataForDelete(String url) {
        ResponseEntity<Note[]> response = restTemplate.getForEntity(url, Note[].class);
        note = response.getBody()[0];
    }

    @When("delete object for {string}")
    public void deleteObjectFor(String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");
        headers.set("X-COM-LOCATION", "USA");

        HttpEntity<Note> requestEntity = new HttpEntity<>(null, headers);

        ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, String.class);
        System.out.println(result.getStatusCodeValue());
        this.code = result.getStatusCodeValue();
    }

    @Then("the response code should be {string}")
    public void theResponseCodeShouldBe(String arg0) {
        Integer b = this.code;
        Assert.assertEquals(arg0,b.toString());
    }

}
